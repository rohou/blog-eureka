package com.blogjlh;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 服务注册中心-启动类
 * Created by jlh on 2017/6/7.
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaStart {
    public static void main(String[] args) {
        new SpringApplicationBuilder(EurekaStart.class).web(true).run(args);
    }
}